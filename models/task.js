// Load required packages
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
// Define our task schema
var TaskSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: { type: String, default: "" },
    deadline: {type: String, required: true},
    completed: { type: Boolean, default: false },
    assignedUser: { type: String, default: "" },
    assignedUserName: { type: String, default: "unassigned" },
    dateCreated: { type: Date, default: Date.now }
});
TaskSchema.plugin(uniqueValidator)
// Export the Mongoose model
module.exports = mongoose.model('Task', TaskSchema);
