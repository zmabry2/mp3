// Load required packages
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
// Define our user schema
var UserSchema = new mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    pendingTasks: {type: [String], default: []},
    dateCreated: { type: Date, default: Date.now }
});
UserSchema.plugin(uniqueValidator)
// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
