const userRouter = require('./users')
const taskRouter = require('./tasks')
module.exports = function (app, router) {
    app.use('/api', require('./home.js')(router));
    app.use('/api/users', userRouter)
    app.use('/api/tasks', taskRouter)
};
