const express = require('express')
const router = express.Router()
const Task = require('../models/task')
const User = require('../models/user')
router.get('/', async (req, res) => {
    let where = req.query.where
    try {
        where = JSON.parse(req.query.where)
    } catch {
        where = req.query.where
    }
    let sort = req.query.sort
    try {
        sort = JSON.parse(req.query.sort)
    } catch {
        sort = req.query.sort
    }
    let select = req.query.select
    try {
        select = JSON.parse(req.query.select)
    } catch {
        select = req.query.select
    }
    let skip = Number(req.query.skip)
    let limit = Number(req.query.limit)
    let count = req.query.count
    console.log(req.query)
    if (count === "true") {
        try {
            const tasks = await Task.find(where).skip(skip).limit(limit).sort(sort).select(select).count()
            res.json({message: "OK", data: tasks})
        } catch (err) {
            res.status(500).json({message: "Server Error", data: [] })
        }
    } else {
        try {
            const tasks = await Task.find(where).skip(skip).limit(limit).sort(sort).select(select)
            res.json({message: "OK", data: tasks})
        } catch (err) {
            res.status(500).json({message: "Server Error", data: [] })
        }
    }
})

router.post('/', async (req, res) => {
    const task = new Task({
        name: req.body.name,
        description: req.body.description,
        deadline: req.body.deadline,
        completed: req.body.completed,
        assignedUser: req.body.assignedUser,
        assignedUserName: req.body.assignedUserName,
        dateCreated: Date.now()
    })
    console.log(req.body)
    try {
        const newTask = await task.save()
        await assignTask("none", newTask, false)
        res.status(201).json({message: "OK", data: newTask})
    } catch (err) {
        console.log(err.message)
        res.status(400).json({message: "Malformed Request", data:[] })
    }
})

router.get('/:id', getTask, (req, res) => {
    res.json({message: "OK", data: res.task})
})

router.put('/:id', getTask, async (req, res) => {
    try {
        const upTask = res.task
        upTask.name = req.body.name
        upTask.description = req.body.description
        upTask.deadline = req.body.deadline
        upTask.completed =  req.body.completed
        upTask.assignedUser = req.body.assignedUser
        upTask.assignedUserName = req.body.assignedUserName
        upTask.save()
        await assignTask("none", upTask, true)
        res.json({message: "OK", data: upTask})
    } catch(err) {
        console.log(err.message)
        res.status(400).json({message: "Malformed Request", data:[]})
    }
})

router.delete('/:id', getTask, async (req, res) => {
    try {
        await assignTask(res.task, "none", false)
        await res.task.remove()
        res.json({ message: 'Task Deleted', data: []})
    } catch(err) { 
        res.status(500).json({message: "Server Error", data:[] })
    }
})

async function getTask(req, res, next) {
    try {
        task = await Task.findById(req.params.id)
        if (task == null) {
            return res.status(404).json({message: "Task ID not found", data:[]})
        }
    } catch(err) {
        return res.status(500).json({message: "Server Error", data:[]})
    }

    res.task = task
    next()
}

/**
 * 
 * @param {*} oldTask 
 * @param {*} newTask 
 * 
 * Handles logic for assigning tasks to users
 * if oldTask is "none", then user only needs to be assigned newTask
 * if newTask is "none", then user only needs oldTask to be removed
 */
async function assignTask(oldTask, newTask, put) {
    console.log(oldTask)
    try {
        // Remove user from oldTask
        if(oldTask != "none") {
            let oldUser = await User.findById(oldTask.assignedUser)
            console.log(oldUser)
            console.log(oldUser.pendingTasks)
            let idx = oldUser.pendingTasks.indexOf(oldTask._id)
            if (idx != -1) {
                oldUser.pendingTasks.splice(idx, 1)
            }
            oldUser.save()
        }
        if(put == true) {
            oldTask = newTask
            if(oldTask.assignedUser != "") {
                let oldUser = await User.findById(oldTask.assignedUser)
                let idx = oldUser.pendingTasks.indexOf(oldTask._id)
                if (idx != -1) {
                    oldUser.pendingTasks.splice(idx, 1)
                }
                oldUser.save()
            }
        }
        if(newTask != "none") {
            console.log(newTask._id)
            if(newTask.assignedUser != "") {
                let user = await User.findById(newTask.assignedUser)
                if(user.pendingTasks.indexOf(newTask._id) == -1 && newTask.completed == false) { //Task doesn't exist and isn't complete
                    user.pendingTasks.push(newTask._id)
                }
                user.save()
            }
        }
    } catch(err) {
        console.log(err.message)
    }
} 
module.exports = router