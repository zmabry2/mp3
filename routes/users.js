const express = require('express')
const router = express.Router()
const User = require('../models/user')
const Task = require('../models/task')
router.get('/', async (req, res) => {
    let where = req.query.where
    try {
        where = JSON.parse(req.query.where)
    } catch {
        where = req.query.where
    }
    let sort = req.query.sort
    try {
        sort = JSON.parse(req.query.sort)
    } catch {
        sort = req.query.sort
    }
    let select = req.query.select
    try {
        select = JSON.parse(req.query.select)
    } catch {
        select = req.query.select
    }
    let skip = Number(req.query.skip)
    let limit = Number(req.query.limit)
    let count = req.query.count
    console.log(req.query)
    if (count === "true") {
        try {
            const users = await User.find(where).skip(skip).limit(limit).sort(sort).select(select).count()
            res.json({message: "OK", data: users})
        } catch (err) {
            res.status(500).json({message: "Server Error", data: [] })
        }
    } else {
        try {
            const users = await User.find(where).skip(skip).limit(limit).sort(sort).select(select)
            res.json({message: "OK", data: users})
        } catch (err) {
            res.status(500).json({message: "Server Error", data: [] })
        }
    }
})

router.post('/', async (req, res) => {
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        pendingTasks: req.body.pendingTasks,
        dateCreated: Date.now()
    })
    console.log(req.body)
    try {
        const newUser = await user.save()
        res.status(201).json({message: "OK", data: newUser})
    } catch (err) {
        res.status(400).json({message: "Malformed Request", data: [] })
    }
})

router.get('/:id', getUser, (req, res) => {
    res.json({message: "OK", data: res.user})
})

router.put('/:id', getUser, async (req, res) => {
    try {
        const upUser = res.user
        upUser.name = req.body.name 
        upUser.email = req.body.email
        upUser.pendingTasks = req.body.pendingTasks
        await upUser.save()
        res.json({message: "OK", data: upUser})
    } catch(err) {
        res.status(400).json({message: "Malformed Request", data: []})
    }
})

router.delete('/:id', getUser, async (req, res) => {
    try {
        await handleUserDeletion(res.user)
        await res.user.remove()
        res.json({ message: 'User Deleted', data: []})
    } catch(err) {
        res.status(500).json({message: "Server Error", data: []})
    }
})

async function getUser(req, res, next) {
    try {
        user = await User.findById(req.params.id)
        if (user == null) {
            return res.status(404).json({message: "User ID not found"})
        }
    } catch(err) {
        console.log(err.message)
        return res.status(500).json({message: "Server Error", data: []})
    }

    res.user = user
    next()
}

async function handleUserDeletion(user) {
    console.log(user)
    try {
        // Remove user from tasks
        user.pendingTasks.forEach(async function(taskId) {
            task = await Task.findById(taskId)
            task.assignedUser = ""
            task.assignedUserName = "unassigned"
            task.save()
        })
    } catch(err) {
        console.log(err.message)
    }
} 

module.exports = router